﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using TMPro;
using UnityEngine.UI;

public class PlayerWeapon : MonoBehaviour
{
    // All the below is just linking references so the script can tweak/reference them, or setting variables. Self explanatory.
    [SerializeField] float weaponRange = 5.0f;
    [SerializeField] float weaponCooldown = 1f;
    [SerializeField] Canvas winCanvas;
    [SerializeField] Camera winCam;
    [SerializeField] AudioSource GiorginioRoar;
    [SerializeField] Animator weaponAnimator;
    [SerializeField] GameObject player;

    [SerializeField] Camera playerCam;
    [SerializeField] TextMeshProUGUI weaponPickupText;

    [SerializeField] public bool canSwing = true;

    [SerializeField] public bool hasWeapon = false;


    // Start is called before the first frame update
    void Start()
    {
        MeshRenderer meshRenderer = gameObject.GetComponent<MeshRenderer>();
        meshRenderer.enabled = false;
        winCanvas.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (CrossPlatformInputManager.GetButtonDown("Fire1") && canSwing == true && hasWeapon == true)
        {
            StartCoroutine(SwingWeapon());
        }
    }
    private IEnumerator SwingWeapon()
    {
        canSwing = false;
        // Plays a swing sound on an attack. 
        // AudioSource playerAudio = GetComponent<AudioSource>();
        // playerAudio.Play();
        // Tells the weapon's animator to play the swing animation, which is where the actual "do damage" portion of the weapon swing is called so that it matches up with the animation a bit better.
        weaponAnimator.Play("axeSwing");
        // Prevents the player from swinging infinitely; must wait a second between each click.
        yield return new WaitForSeconds(weaponCooldown);
        canSwing = true;
    }

    public void WeaponRaycast()
    {
        RaycastHit hit;
        if (Physics.Raycast(playerCam.transform.position, playerCam.transform.forward, out hit, weaponRange))
        {
            if (hit.transform.gameObject.tag == "Enemy")
            {
                GiorginioRoar.Play();
                winCanvas.enabled = true;
                winCam.enabled = true;
                player.SetActive(false);
                hit.transform.gameObject.SetActive(false);
            }
        }
    }

}
