﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class GiorgoPathfinding : MonoBehaviour
{
    [SerializeField] Transform playerTarget;
    [SerializeField] GameObject currentWaypoint;
    List<GameObject> nodeChoices = new List<GameObject>();
    List<GameObject> nodesTravelled = new List<GameObject>();
    [SerializeField] GameObject groundNode;
    [SerializeField] GameObject topNode;
    [SerializeField] public bool playerGround;
    [SerializeField] bool enemyGround;
    EnemyFieldOfView enemyFOV;
    Animator giorgioAnimator;
    GameObject nextNode;
    AudioSource jumpscareRoar;
    private NavMeshAgent nMA;
    int nextNodeInt;

    void Start()
    {
        enemyFOV = GetComponent<EnemyFieldOfView>();
        nMA = GetComponent<NavMeshAgent>();
        giorgioAnimator = GetComponentInChildren<Animator>();
        jumpscareRoar = GetComponentInChildren<AudioSource>();
    }

    void Update()
    {
        Transform waypointTransform = currentWaypoint.transform;
        if (Vector3.Distance(transform.position, waypointTransform.position) < 1)
        {
            nodesTravelled.Add(currentWaypoint);
            currentWaypoint = FindNextWayPoint();
        }
        if (enemyFOV.playerVisible == true)
        {
            nMA.SetDestination(playerTarget.position);
            nMA.speed = 7;
            jumpscareRoar.Play();
            giorgioAnimator.SetBool("isChasing", true);
            
        }
        else if (enemyFOV.playerVisible == false)
        {   
            nMA.SetDestination(waypointTransform.position);
            nMA.speed = 3;
            giorgioAnimator.SetBool("isChasing", false);
        }
    }

    GameObject FindNextWayPoint()
    {
        int nodeAmount = nodesTravelled.Count;
        bool sameFloor = DetectFloorMovement();
        WaypointDetection waypointNodes = currentWaypoint.GetComponent<WaypointDetection>();
        if (sameFloor == true)
        {
            foreach (GameObject node in waypointNodes.nearbyWaypoints)
            {
                if (nodeAmount > 1)
                {
                    if ((nodesTravelled[nodeAmount - 2] != node))
                    {
                        nodeChoices.Add(node);
                    }
                }
                else
                {
                    nodeChoices.Add(node);
                }
            }
        }
        else if (sameFloor == false)
        {
            if (playerGround == false)
            {
                return topNode;
            }
            else if (playerGround == true)
            {
                return groundNode;
            }
        }

        nextNodeInt = Random.Range(0, nodeChoices.Count);
        nextNode = nodeChoices[nextNodeInt];
        nodeChoices.Clear();
        return nextNode;
    }

    bool DetectFloorMovement()
    {
        WaypointDetection currentNode = currentWaypoint.GetComponent<WaypointDetection>();
        if (currentNode.isGroundFloor == playerGround)
        {
            return true;
        }
        else
        {
            return false;
        }

    }
}
