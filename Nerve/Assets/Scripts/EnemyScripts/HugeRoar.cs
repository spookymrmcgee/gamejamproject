﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HugeRoar : MonoBehaviour
{
    [SerializeField] AudioSource roar;

    void Roar()
    {
        roar.Play();
    }
}
