﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpscareRoa : MonoBehaviour
{
    [SerializeField] TutorialChaseMonster tutorialChaseMonster;
    [SerializeField] AudioSource hugoRoar;
    [SerializeField] AudioSource music;
    void Roar()
    {
        music.Stop();
        hugoRoar.Play();
    }
}
