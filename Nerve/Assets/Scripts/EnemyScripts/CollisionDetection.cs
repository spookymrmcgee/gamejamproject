﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetection : MonoBehaviour
{
    [SerializeField] bool bottom;
    [SerializeField] bool top;
    [SerializeField] GiorgoPathfinding enemyPathfinding;
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (bottom)
            {
                enemyPathfinding.playerGround = true;
            }
            else if (top)
            {
                enemyPathfinding.playerGround = false;
            }
        }
    }
}
