﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class KillScript : MonoBehaviour
{
    [SerializeField] GameObject player;
    [SerializeField] Camera jumpScareCam;
    [SerializeField] Animator jumpscareAnim;
    [SerializeField] PlayerWeapon axe;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && axe.hasWeapon == false)
        {
            player.SetActive(false);
            jumpScareCam.enabled = true;
            jumpscareAnim.SetBool("playerKilled", true);
            StartCoroutine(ReloadLevel());
        }
    }

    private IEnumerator ReloadLevel()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(2);
    }
}
