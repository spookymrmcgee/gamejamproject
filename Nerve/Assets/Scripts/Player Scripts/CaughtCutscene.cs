﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaughtCutscene : MonoBehaviour
{

    [SerializeField] GameObject hugo;
    private TutorialChaseMonster chaseScript;

    [SerializeField] Animation caughtAnim;

    [SerializeField] Animation hugoRage;

    [SerializeField] float CutsceneDelay = 1f;

    // Start is called before the first frame update
    void Start()
    {
        chaseScript = hugo.GetComponent<TutorialChaseMonster>();
    }

    // Update is called once per frame
    void Update()
    {
        //if(chaseScript.caught == true)
        //{
        //    StartCoroutine(startCutscene());
        //}
        //else
        //{
        //    return;
        //}
    }

    private void DoTheThing()
    {
        StartCoroutine(startCutscene());
    }

    IEnumerator startCutscene()
    {
        caughtAnim.Play();
        yield return new WaitForSeconds(CutsceneDelay);
        hugoRage.Play();

    }
}
