﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class DoorInteract : MonoBehaviour
{
    [SerializeField] float interactRange = 7.0f;
    [SerializeField] Camera playerCam;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (CrossPlatformInputManager.GetButtonDown("Fire1"))
        {
            OpenRaycast();
        }
    }

    private void OpenRaycast()
    {
        RaycastHit hit;
        if (Physics.Raycast(playerCam.transform.position, playerCam.transform.forward, out hit, interactRange))
        {
            doorScript door = hit.transform.GetComponent<doorScript>();
            blueDoor blueDoor = hit.transform.GetComponent<blueDoor>();
            redDoor redDoor = hit.transform.GetComponent<redDoor>();
            if (door == null && blueDoor == null && redDoor == null)
            {
                return;
            }
            if (door != null)
            {
                door.OnInteract();
            }
            if (blueDoor != null)
            {
                blueDoor.OnInteract();
            }
            if (redDoor != null)
            {
                redDoor.OnInteract();
            }
            print("doortest");
        }
    }
}
