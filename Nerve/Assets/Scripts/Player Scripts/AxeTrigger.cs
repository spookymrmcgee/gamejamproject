﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxeTrigger : MonoBehaviour
{
    [SerializeField] PlayerWeapon playerWeapon;
    [SerializeField] GameObject handAxe;
    [SerializeField] QuestLog questLog;
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            questLog.UpdateLog(5);
            playerWeapon.hasWeapon = true;
            MeshRenderer weaponRenderer = gameObject.GetComponent<MeshRenderer>(); 
            MeshRenderer axeMeshRenderer = handAxe.GetComponent<MeshRenderer>();
            axeMeshRenderer.enabled = true;
            weaponRenderer.enabled = false;
        }
    }
}
