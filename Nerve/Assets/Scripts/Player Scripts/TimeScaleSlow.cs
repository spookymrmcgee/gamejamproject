﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;
public class TimeScaleSlow : MonoBehaviour
{

    private float fixedDeltaTime;
    [SerializeField] float slowAmount = 0.5f; //How much the game will slow when SlowTime is called. 0.5 is x2 slower 0.25 is x4 slower ect.
    private float normalTimeScale = 1f; //The normal flow of time
    float slowDuration = 10f;

    [SerializeField] float zoomIn = 80f;
    [SerializeField] float zoomOut = 60f;

    [SerializeField] Camera playerCamera;

    [SerializeField] TextMeshProUGUI JumpText;

    [SerializeField] GameObject BridgeBarrier;
    private Collider bridgeCollider;

    [SerializeField] TextMeshProUGUI SlideText;

    [SerializeField] GameObject SlideBarrier;
    private Collider slideCollider;
    [SerializeField] GameObject Tree;
    private Collider treeCollider;

    [SerializeField] TextMeshProUGUI SprintText;
    public bool SprintTriggered = false;

    [SerializeField] bool jumpTutorialing = false;
    bool slideTutorialing = false;

    
    [SerializeField] Animator TreeAnim;
    [SerializeField] Animator HugoAnim;
 
    //float interpolateSlowAmount = 1f;

    void Awake()
    {
        this.fixedDeltaTime = Time.fixedDeltaTime; //Copy of the current time, so the script can keep track of it.
    }

    private void Start()
    {
        bridgeCollider = BridgeBarrier.GetComponent<Collider>();
        slideCollider = SlideBarrier.GetComponent<Collider>();
        treeCollider = Tree.GetComponent<Collider>();

        
        
    }
    // Update is called once per frame
    void Update()
    {
        if (jumpTutorialing)
        {
            
            if(Input.GetKeyDown("space"))
            {
                
                CorrectInputJump();
            }
        }

        if(slideTutorialing)
        {
            if(Input.GetKeyDown("q"))
            {
                CorrectInputSlide();
            }
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name=="JumpTutorialTrigger")
        {
            //SlowTime();
            
            jumpTutorialing = true;
        }

        if(other.gameObject.name=="SlideTutorialTrigger")
        {
            //SlowTime();

            slideTutorialing = true;
        }

        if(other.gameObject.name=="SprintingTutorialTrigger")
        {
            SprintText.enabled = true;
            SprintTriggered = true;
        }
        
        if(other.gameObject.name== "TreeTrigger")
        {
            TreeAnim.SetBool("TreeFall", true);
        }


    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.name=="JumpTutorialTrigger")
        {
            
           
            JumpText.enabled = false;

            jumpTutorialing = false;

            ReturnTime();
        }

        if (other.gameObject.name == "SlideTutorialTrigger")
        {
            ReturnTime();

            SlideText.enabled = false;

            slideTutorialing = false;
        }

        if (other.gameObject.name == "SprintingTutorialTrigger")
        {
            SprintText.enabled = false;
        }

    }

    private void CorrectInputJump()
    {
        bridgeCollider.enabled = false;
        
        JumpText.enabled = false;
        jumpTutorialing = false;
        ReturnTime();
    }

    private void CorrectInputSlide()
    {
        ReturnTime();
        
        SlideText.enabled = false;

        slideTutorialing = false;

        slideCollider.enabled = false;
        treeCollider.enabled = false;

    }

    private void SlowTime()
    {
        Time.timeScale = Mathf.Lerp(normalTimeScale, slowAmount, slowDuration );
        Time.timeScale = slowAmount;
        
        playerCamera.fieldOfView = zoomIn; //Zoom in
    }

    private void ReturnTime()
    {
        Time.timeScale = Mathf.Lerp(slowAmount, normalTimeScale, slowDuration);


        playerCamera.fieldOfView = zoomOut; //Zoomes Out
    }

 

       
}
