﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroCutscene : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] Camera introCam;
    [SerializeField] GameObject playerObject;

    [SerializeField] Animation introAnim;
    [SerializeField] AudioSource aS;

    [SerializeField] AudioClip chaseAudio;

    [SerializeField] float CutsceneDelay;
    [SerializeField] float musicDelay;
    bool musicPlayed = false;
    void Start()
    {
        introCam.enabled = true;
        playerObject.SetActive(false);
        StartCoroutine(startMusic());
        StartCoroutine(startIntro());
        
        aS = GetComponent<AudioSource>();
    }

    IEnumerator startIntro()
    {
        introAnim.Play();
        yield return new WaitForSeconds(CutsceneDelay);
        introCam.enabled = false;
        playerObject.SetActive(true);
    }

    IEnumerator startMusic()
    {
        yield return new WaitForSeconds(musicDelay);
        aS.PlayOneShot(chaseAudio);
        musicPlayed = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
