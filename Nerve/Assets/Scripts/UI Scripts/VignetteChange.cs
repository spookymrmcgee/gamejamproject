﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.Rendering;


public class VignetteChange : MonoBehaviour
{
    private PostProcessVolume volume;
    private Vignette vignette;
    private Color red = new Color(1f, 0, 0, 1f);

    

    [SerializeField] GameObject player;
    private TimeScaleSlow triggerScript;


    // Start is called before the first frame update
    void Start()
    {
       

        volume = GetComponent<PostProcessVolume>();
        volume.profile.TryGetSettings(out vignette);
        vignette.color.Override(Color.black);

        triggerScript = player.GetComponent<TimeScaleSlow>();

    }

    // Update is called once per frame
    void Update()
    {
        if(triggerScript.SprintTriggered)
        {
            ChangeVignette();
        }

        else
        {
            vignette.color.Override(Color.black);
            vignette.intensity.Override(0.625f);
        }

    }

    

    private void ChangeVignette()
    {
        vignette.color.Override(red);
        vignette.intensity.Override(0.725f);
    }
}
