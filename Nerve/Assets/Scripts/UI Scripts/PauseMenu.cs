﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] Canvas PauseCanvas;
    [SerializeField] PlayerMovement playerMovement;


    // Start is called before the first frame update
    void Start()
    {
        PauseCanvas.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (CrossPlatformInputManager.GetButtonDown("Cancel"))
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            PauseGame();
        }
    }

    public void PauseGame()
    {
        playerMovement.enabled = false;
        Time.timeScale = 0;
        PauseCanvas.enabled = true;
    }

    public void UnpauseGame()
    {
        playerMovement.enabled = true;
        Time.timeScale = 1;
        PauseCanvas.enabled = false;
    }

    public void ExitGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}
