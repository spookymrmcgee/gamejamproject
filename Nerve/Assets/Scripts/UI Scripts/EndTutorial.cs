﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class EndTutorial : MonoBehaviour
{

    [SerializeField] GameObject loadingScreen;
    [SerializeField] Slider slider;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "EnterHouseTrigger")
        {
            LoadLevel(2);
        }
    }

    public void LoadLevel(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronolously(sceneIndex));
        
    }

    IEnumerator LoadAsynchronolously(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        loadingScreen.SetActive(true);

        while(!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            slider.value = progress;

            yield return null;
        }
    }

}
