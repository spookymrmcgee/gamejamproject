﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TutorialMessages : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI JumpText;
    [SerializeField] TextMeshProUGUI SlideText;
    [SerializeField] TextMeshProUGUI SprintText;

    // Start is called before the first frame update
    void Awake()
    {
        JumpText.enabled = false;
        SlideText.enabled = false;
        SprintText.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "JumpTutorialTrigger")
        {
            JumpText.enabled = true;
        }

        if(other.gameObject.name == "SlideTutorialTrigger")
        {
            SlideText.enabled = true;
            
        }
    }

}