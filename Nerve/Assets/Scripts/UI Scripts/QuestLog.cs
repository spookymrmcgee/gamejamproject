﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class QuestLog : MonoBehaviour
{
    //[SerializeField] List<string> questlogList = new List<string>();
    [SerializeField] public string[] questArray;
    [SerializeField] TextMeshProUGUI questText;

    [SerializeField] public int questlogListIndex = 0;

    private void Start()
    {
        questlogListIndex = 0;
    }

    void Update()
    {
        questText.text = questArray[questlogListIndex];
    }

    public void UpdateLog(int indexUpdate)
    {
        questlogListIndex = indexUpdate;
    }
}
