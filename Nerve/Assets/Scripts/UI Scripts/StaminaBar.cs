﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class StaminaBar : MonoBehaviour
{
    public TextMeshProUGUI staminaText;
    public Slider staminaSlider;
    [SerializeField] bool fadeIn;
    [SerializeField] bool fadeOut;
    [SerializeField] private CanvasGroup staminaCanvas;
    public void SetMaxStamina(float maxStamina)
    {
        staminaSlider.maxValue = maxStamina;
        staminaSlider.value = maxStamina;
    }

    public void SetStamina(float stamina)
    {
        staminaSlider.value = stamina;
    }

    public void ShowUI()
    {
        fadeIn = true;
        fadeOut = false;
    }

    public void HideUI()
    {
        fadeOut = true;
        fadeIn = false;
    }

    private void Update()
    {
        if (fadeIn)
        {
            if (staminaCanvas.alpha < 1)
            {
                staminaCanvas.alpha += Time.deltaTime;
                if (staminaCanvas.alpha >= 1)
                {
                    fadeIn = false;
                }
            }
        }

        if (fadeOut)
        {
            if (staminaCanvas.alpha >= 0)
            {
                staminaCanvas.alpha -= Time.deltaTime;
                if (staminaCanvas.alpha == 0)
                {
                    fadeOut = false;
                }
            }
        }
    }
}
