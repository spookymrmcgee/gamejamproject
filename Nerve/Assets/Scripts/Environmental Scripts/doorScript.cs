﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorScript : MonoBehaviour
{
    bool isOpen = false;
    Animator doorAnims;

    // Start is called before the first frame update
    void Start()
    {
        doorAnims = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnInteract()
    {
        if(isOpen == false)
        {
            PlayOpen();
        }
        else if(isOpen == true)
        {
            PlayClose();
        }
    }

    public void PlayOpen()
    {
        isOpen = true;
        doorAnims.SetBool("doorOpen", true);
    }

    public void PlayClose()
    {
        isOpen = false;
        doorAnims.SetBool("doorOpen", false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            if (isOpen == false)
            {
                PlayOpen();
            }
        }
    }
}
