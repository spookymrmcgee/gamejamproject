﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class redDoor : MonoBehaviour
{
    bool isOpen = false;
    Animator doorAnims;
    bool firstTimeOpen = true;

    [SerializeField] GameObject player;
    [SerializeField] QuestLog questLog;
    // Start is called before the first frame update
    void Start()
    {
        doorAnims = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnInteract()
    {
        if (isOpen == false && player.GetComponent<PlayerItems>().playerItems.Contains("Red Key"))
        {
            PlayOpen();
        }
        else if (isOpen == true)
        {
            PlayClose();
        }
    }

    public void PlayOpen()
    {
        isOpen = true;
        if (firstTimeOpen == true && questLog.questlogListIndex == 1)
        {
            questLog.UpdateLog(2);
        }
        firstTimeOpen = false;
        doorAnims.SetBool("doorOpen", true);
    }

    public void PlayClose()
    {
        isOpen = false;
        doorAnims.SetBool("doorOpen", false);
    }
}
