﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class TutorialChaseMonster : MonoBehaviour
{

    //[SerializeField] Animator TreeAnim;
    public PlayableDirector playableDirector;
    [SerializeField] GameObject player;
    [SerializeField] Camera jumpScareCam;
    [SerializeField] Light Moon;
    [SerializeField] Animator hugoKillAnimator;
    [SerializeField] AudioSource hugoRoar;

    public bool caught = false;

    // Start is called before the first frame update
    void Start()
    {
        playableDirector.Stop();
    }

    // Update is called once per frame
   
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            CaughtPlayer();
            caught = true;
        }
    }

    private void CaughtPlayer()
    {
        player.SetActive(false);
        jumpScareCam.enabled = true;
        hugoKillAnimator.SetBool("playerDead", true);
        StartCoroutine(ReloadLevel());
    }



    private IEnumerator ReloadLevel()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene(1);
    }

    public void PlayTimelines()
    {
        playableDirector.Play();
        Moon.enabled = false;
    }
}
