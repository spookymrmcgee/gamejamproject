﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ItemInteraction : MonoBehaviour
{
    [SerializeField] public ItemData itemInfo;
    [SerializeField] PlayerItems playerItems;
    [SerializeField] TextMeshProUGUI pickupText;
    [SerializeField] QuestLog questLog;
    private void Awake()
    {
        pickupText.enabled = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            playerItems.playerItems.Add(itemInfo.itemType);
            if (itemInfo.itemType == "Red Key")
            {
                questLog.UpdateLog(1);
            }
            if (itemInfo.itemType == "Blue Key")
            {
                questLog.UpdateLog(3);
            }
            pickupText.text = (itemInfo.flavourText);
            StartCoroutine(displayText());
        }
    }

    IEnumerator displayText()
    {
        pickupText.enabled = true;
        yield return new WaitForSeconds(3);
        pickupText.enabled = false;
        gameObject.SetActive(false);
    }
}
