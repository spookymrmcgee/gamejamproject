﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class ItemData : ScriptableObject
{
    [SerializeField] public bool isPickedUp = false;
    [SerializeField] public string itemType;
    [SerializeField] public string flavourText;
    // Start is called before the first frame update
    void Start()
    {
        isPickedUp = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
