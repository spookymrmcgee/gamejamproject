﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Giorgio_Footsteps : MonoBehaviour
{
    #region Variables
    [Header("Audio files")]
    [SerializeField] private AudioClip _leftFoot;
    [SerializeField] private AudioClip _rightFoot;

    [Header("Audio Source")]
    private AudioSource _audioSource;
    #endregion

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void RightFootSound()
    {
        _audioSource.PlayOneShot(_rightFoot);
    }

    public void LeftFootSound()
    {
        _audioSource.PlayOneShot(_leftFoot);
    }



}
