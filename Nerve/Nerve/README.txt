CONTROLS:
WASD for movement
SPACE BAR for jump
Q for slide
LEFT CTRL for crouch
LEFT SHIFT for sprint

CREDITS:
Aaron Bott - Project Lead, Head Coder and Source Manager
Louise Fowler - Co-Lead, Head Artist and Sound Designer
Leviathan Stevens - Monster Artist & Animator
Matuesz Korcipa - Coder
Mario Pastrana - Coder
Kelly Cook - Environmental Artist

Additional credits to Andrew for the house scene OST, and PIXABAY for sample sounds